import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './components/Home';
import SecuredPage from './testComponents/SecuredPage';
import Navigation from './components/Navigation';
import {useAuth0} from "@auth0/auth0-react";
import {useEffect, useState} from "react";
import axios from "axios";
import {register} from "./services/userApi";

const App = () => {
    document.title = "Callmemaybe";

    const { isLoading, getAccessTokenSilently, isAuthenticated, loginWithRedirect } = useAuth0();
    const [isAccessTokenSet, setIsAccessTokenSet] = useState(false);
    const [token, setToken] = useState("");

    if (!isAuthenticated && !isLoading) {
        loginWithRedirect();
    }

    const getAccessToken = async () => {
        const accessToken = await getAccessTokenSilently();
        setToken(accessToken);
        console.log(accessToken)
        axios.interceptors.request.use((config) => {
            if (config && config.headers) {
                config.headers["Authorization"] = `Bearer ${accessToken}`;
            }
            return config;
        });
        setIsAccessTokenSet(true);
    };

    useEffect(() => {
        if (isAuthenticated) {
            getAccessToken();
        }
    }, [isAuthenticated]);

    useEffect(() => {
        if (isAuthenticated && isAccessTokenSet) {
            register();
        }
    }, [isAuthenticated, isAccessTokenSet])

    if (isLoading || (!isLoading && isAuthenticated && !isAccessTokenSet)) {
        return <></>;
    }

  return (
    <div className={"flex flex-col h-screen"}>
      <BrowserRouter>
        <Navigation />
          <main>
              <Routes>
                  <Route path="/" element={<Home  token={token}/>} />
                  <Route path="/secured" element={<SecuredPage />} />
              </Routes>
              {/* <Footer/> */}
          </main>
      </BrowserRouter>
    </div>
  );
}

export default App;
