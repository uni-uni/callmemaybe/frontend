import {useEffect, useState} from "react"
import MessageGrid from "./MessageGrid"
import SearchComponent from "./SearchComponent"
// import UsersList from "./UsersList"
import {getConnections, getGroups, getMessages} from "../services/userApi";


function MessageComponent(props) {
    const [groupId, setGroupId] = useState(null)
    const [groups, setGroups] = useState([])

        return(
            <div className={"grid grid-cols-4 w-full"}>
                <div>
                <SearchComponent groups={groups} groupId={groupId} setGroupId={setGroupId} />
                </div>
                <div className={"col-span-3"}>
                    {console.log(groups)}
                    <MessageGrid groupId={groupId} groups={groups} token={props.token}>
                    </MessageGrid>
                </div>
            </div>
        )

}

export default MessageComponent