/** TODO CHANGE WIDTH */

import { useAuth0 } from "@auth0/auth0-react";

const MessagesList = (props) => {

    const {user} = useAuth0()
    // console.log(props.messages)

    return (
        <ul style={{ height: '65vh' }} className={"grid scroll-auto overflow-y-scroll min-h-min"}>
            {props.messages.map((message, idx) => (

                <li key={`message_${idx}`} className={message.email === user.email ? "bg-blue-300 rounded border-double border-2 border-blue-400 w-40 mb-2 justify-self-end" : "bg-gray-300 w-40 my-2 justify-self-start rounded border-double border-2 border-gray-400"}>
                    {message.messages.content}
                </li>
            ))}
        </ul>
    )
}

export default MessagesList;