import React from "react";
import MessageComponent from "./MessageComponent";
import MessageGrid from "./MessageGrid";
import UsersList from "./UsersList";

const Home = (props) => {
    return (
        <MessageComponent token={props.token} />
    )
}

export default Home;