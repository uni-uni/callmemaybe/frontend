import {Tab} from '@headlessui/react'
import UsersList from "./UsersList"
import SearchUsers from "./SearchUsers"

const SearchComponent = (props) => {
    const classNames = (...clasess) => {
        return clasess.filter(Boolean).join(' ')
    }
    // console.log(props.users)

    const tabList = [
        'Message',
        'Friend list',
        'Invitation'
    ]

    return (
        <Tab.Group>
          <Tab.List className="flex space-x-1 rounded-xl bg-blue-900/20 p-1">
            {tabList.map((item) => (
                <Tab key={item} className={({ selected }) =>
                classNames(
                  'w-full rounded-lg py-2.5 text-sm font-medium leading-5 text-blue-700',
                  'ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2',
                  selected
                    ? 'bg-white shadow'
                    : 'text-blue-100 hover:bg-white/[0.12] hover:text-white'
                )
              }>{item}</Tab>
            ))}
          </Tab.List>
          <Tab.Panels className="mt-2">
            <Tab.Panel>
                <UsersList groups={props.groups} users={props.users} groupId={props.groupId} setGroupId={props.setGroupId} />
            </Tab.Panel>
            <Tab.Panel>
                <SearchUsers/>
            </Tab.Panel>
          </Tab.Panels>
        </Tab.Group>
      )
}

export default SearchComponent