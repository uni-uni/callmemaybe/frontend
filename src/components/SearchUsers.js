
import { useState } from "react"
import { getAllUsers } from "../services/userApi"
import InviteUserComponent from "./InviteUserComponent"
import SearchBox from "./SearchBox"

const SearchUsers = (props) => {

    const [user, setUser] = useState({})
    const [flag, setFlag] = useState(false)


    const emails = [
        "test@gmail.com",
        "franz@gmail.com",
        "a22@onet.pl"
    ]

    const searchingFunc = (email) => {
        if (email === "") {
            setUser([])
            setFlag(false)
        }
        getAllUsers().then((data) => {
            const tempUser = data.find(e => e.email === email)
            setUser({
                email: tempUser.email,
                name: tempUser.username,
                find: true,
                id: tempUser.user_id
            })
        })
        if (user.find) {
            setFlag(true)
        }
    }

    return (
        <div style={{ height: '87vh' }}>
            <aside className="h-full" aria-label="Sidebar">
                <SearchBox placeholder={"search user e.g example@email.com"} searchingFunc={searchingFunc} />
                <div className="px-3 w-full h-full bg-red py-4 overflow-y-scroll rounded bg-gray-50 dark:bg-gray-800">

                    <InviteUserComponent email={user.email} flag={flag} name={user.name} />
                </div>
            </aside>
        </div>
    )
}

export default SearchUsers