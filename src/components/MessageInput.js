import { useEffect, useRef, useState } from "react"


export const MessageInput = (props) => {

    const [message, setMessage] = useState("")
    const [test, setTest] = useState(null)

    const ws = useRef(null)

    useEffect(() => {
        console.log("MessageInput")
        console.log(props.groupId)
        ws.curent = new WebSocket(process.env.REACT_APP_API_HOST + "/" + props.groupId); //zmienic groupId
        ws.curent.onopen = () => {
            console.log("ws onopen");
            const msg = {
                text: " ",
                token: props.token
            };
            ws.curent.send(JSON.stringify(msg));
        }
        ws.curent.onclose = () => console.log("ws onclose");
        ws.curent.onmessage = (e) => {
            console.log(JSON.parse(e.data));
            props.setMessages(messages => [...messages, JSON.parse(e.data)])
        };

        const currentWS = ws.curent;
        return () => currentWS.close();
    }, [props.groupId]);

    const handleSendMessage = () => {
        if (message.length > 0) {
            const msg = {
                type: "content",
                text: message,
                token: props.token
            };
            ws.curent.send(JSON.stringify(msg));
            setMessage("")
        }
    };

    return (
        <div className={"flex flex-col"}>
            <div>
            <label htmlFor="message" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your message</label>
            <textarea id="message" rows="2" className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" value={message} onChange={(e) => setMessage(e.target.value)} placeholder="Write your message here..."></textarea>
            </div>
            <div>
            <button type="submit" className="text-white right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" onClick={handleSendMessage}>Send</button>
            </div>
        </div>

    )
}