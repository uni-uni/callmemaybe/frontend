import { useEffect, useState } from "react"
import {getConnections, getGroups, getMessages} from "../services/userApi"
import SearchBox from "./SearchBox"
import axios from "axios"

const UsersList = (props) => {

    const [activeUser, setActiveUser] = useState(0)
    const [groups, setGroups] = useState([])
    const [messages, setMessages] = useState([])
    
    useEffect(() => {
        try{
            getGroups().then((data) => {
                setGroups(data)
            })
            getMessages(props.groupId).then((data) => setMessages(data))
        }catch(e){
            
        }
        // console.log(props.groupId)
    }, [])

    const getProperDate = (date) => {
        const currentDate = new Date()
        if (currentDate.getDate() === date.getDate() && currentDate.getMonth() === date.getMonth() && currentDate.getFullYear() === date.getFullYear()) {
            return `${date.getHours()}:${date.getMinutes()}`
        } else {
            return `${date.getDate()}.${getProperMonth(date.getMonth())}`
        }
    }

    const getProperMonth = (month) => {
        if (month < 10) {
            return `0${month + 1}`
        } else {
            return `${month + 1}`
        }
    }

    const getLastMessage = () => {
        messages.sort((a, b) => new Date(a.messages.sending_date) - new Date(b.messages.sending_date))
        messages.reverse()
        const message = messages[0]?.messages.content
        if (message?.length > 28) {
            return `${message.substring(0, 25)}...`
        } else {
            return message
        }
    }


    const getUsersList = () => {
        if(messages?.length >= 0 && groups?.length >= 0) {
            // {console.log(groups[0][0].group_id)}
            return (
                groups.map((group) => (
                    <div key={group[0].group_id}>
                        <button onClick={() => {
                                props.setGroupId(group[0].group_id)
                            }
                        }>
                            <div className={activeUser === group[0].group_id ? "bg-gray-200 grid grid-rows-2 gap-2 w-full items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700" : "grid grid-rows-2 gap-2 w-full items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"}>
                                <div className={"w-full flex"}>
                                    <div className={"text-l font-semibold text-left"}>
                                        {group[0].group_name}
                                    </div>
                                    <div className={" ml-5 flex items-center justify-center"}>
                                    </div>
                                </div>
                                <div className={"text-base w-full flex "}>
                                    <div className={"w-52 text-left"}>
                                        {getLastMessage()}
                                    </div>
                                    <div className={""}>
                                        {/* {getProperDate(user.date)} */}
                                        date
                                    </div>
                                </div>
                            </div>
                        </button>
                    </div>
                ))
            )
        }
       
    }

    return (
        <div style={{ height: '87vh' }}>
            <aside className="h-full" aria-label="Sidebar">
                <SearchBox placeholder={"search friend"} />
                <div className="px-3 w-full h-full bg-red py-4 overflow-y-scroll rounded bg-gray-50 dark:bg-gray-800">
                    {getUsersList()}
                </div>
            </aside>
        </div>
    )
}



export default UsersList;