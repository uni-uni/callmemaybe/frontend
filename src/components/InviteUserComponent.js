import { useEffect } from "react"
import { useState } from "react"

const InviteUserComponent = (props) => {

    const [name, setName] = useState("")
    const [email, setEmail] = useState("")

    useEffect(() => {
        setName(props.name)
        setEmail(props.email)
    })


    if (!props.flag) {
        return (
            <div />
        )
    } else {
        return (
            <div className="grid gap-2 w-full items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                <div className={"w-full flex flex-row"}>
                    <div className="flex flex-col">
                        <div>
                            {name}
                        </div>
                        <div>
                            {email}
                        </div>
                        <div>
                        </div>
                    </div>
                    <div className={"ml-20 -mt-2 flex items-end"} >
                        {/* Mały szacher macher bedzie tu */}
                    <button type="submit" className="text-white right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" onClick={() => setName("Anna")}>Connect</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default InviteUserComponent