// import { useState } from "react"

import { useEffect, useState } from "react"
import { getMessages } from "../services/userApi"
import MessagesList from "./MessagesList"
import axios from "axios"
import { MessageInput } from "./MessageInput"

const MessageGrid = (props) => {

    const MILLISECONDS_IN_MINUTE = 60000


    const userDetails = {
        name: "Anna",
        active: true,
        lastActive: new Date(),
    }

    const [messages, setMessages] = useState([])

    useEffect(() => {
        getMessages(props.groupId).then((data) => {
            setMessages(data);
            // console.log(data);
        })
    }, [props.groupId])

    const activeStatus = () => {
        if (userDetails.active) {
            return "Active now"
        } else {
            const diff = (new Date() - userDetails.lastActive) / MILLISECONDS_IN_MINUTE
            if (diff < 60) {
                return `Last seen ${diff} minutes ago`
            } else if (diff < 60 * 24) {
                return `Last seen: ${userDetails.lastActive.getHours()}:${userDetails.lastActive.getMinutes()}`
            } else {
                return `Last seen: ${userDetails.lastActive.getDate()}.${userDetails.lastActive.getMonth()}`
            }
        }
    }


    if (messages?.length >= 0) {
        return (
            <div style={{ height: '80vh' }} className={"flex flex-col"}>
                <div className={"flex flex-col gap-2 border-b-2 border-gray-800 max-h-20"}>
                    <div className={" grid items-start justify-left ml-5 text-xl font-semibold"}>

                        {/*{console.log(props.groups)}*/}
                        {props.groupId}
                    </div>
                    <div className={"grid justify-left ml-5 mb-3 text-base"}>
                        {activeStatus()}
                    </div>
                </div>
                <div className={"container w-5/6 mx-auto mt-2"}>
                    <MessagesList messages={messages} />
                </div>
                <div className={"inline-block align-bottom"}>
                    <MessageInput token={props.token} setMessages={setMessages} groupId={props.groupId} />
                </div>
            </div>
        )
    }
}

export default MessageGrid;