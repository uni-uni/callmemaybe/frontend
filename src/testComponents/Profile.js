import React from "react";
import { useAuth0 } from "@auth0/auth0-react";

const Profile = () => {
    const {isAuthenticated} = useAuth0();

    if( isAuthenticated) {
        return(
            <div>
                Is Authenticated
            </div>
        )
    }else {
        return(
            <div>
                No access
            </div>
        )
    }
}

export default Profile;