import React, {useEffect, useRef, useState} from "react";
import {getGroups} from "../services/userApi";
import {useAuth0} from "@auth0/auth0-react";
const SecuredPage = (props) => {
    const { user} = useAuth0();

    return (
        <div>
            {user.email}
            <img src={user.picture}/>
        </div>
    )
}

export default SecuredPage;