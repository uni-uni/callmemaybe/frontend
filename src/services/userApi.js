import {getAPICall, postAPICall, postNoDataAPICall} from "./apiHandler";


export const getGroups = async () =>
{
    const res = await getAPICall("/api/v1/groups/all");
    return res.data;
}

export const getInvitations = async () =>
{
    const res = await getAPICall("/api/v1/users/invitations/all");
    return res.data;
}

export const sendInvitation = async (data) =>
{
    return postAPICall("/api/v1/users/invitations/change_status", data);
}

export const getConnections =  async () => {
    const res =  await getAPICall("/api/v1/users/connections");
    return res.data;
}

export const getMessages = async (id) => {
    const res =  await getAPICall(`/api/v1/users/messages/${id}`);
    return res.data;
}

export const getAllUsers = async () => {
    const res = await getAPICall('/api/v1/users/all');
    return res.data;
}

export const inviteUserToFriends = async (email) => {
    postAPICall('/api/v1/users/invitations/send', {
        user_email_to_add: email
    }).then((response) => {
        console.log(response)
        alert("User has been invited")
    }).catch((error) => {
        console.log(error)
        alert("Ooops something goes wrong, check console for details")
    })
}

export const register = async () => {
    postNoDataAPICall(`/api/v1/users/register`);
}