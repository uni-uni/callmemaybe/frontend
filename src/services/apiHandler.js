import axios from "axios";

const url = process.env.REACT_APP_API_URL

export const getAPICall = async (path) => {
    return await axios.get(url + path);
}

export const postAPICall = (path, data) => {
    return axios.post(url + path, data);
}

export const postNoDataAPICall = (path) => {
    return axios.post(url + path);
}

export const putAPICall = (data) => {
    return axios.put(url, data);
}

export const deleteAPICall = () => {
    return axios.delete(url);
}
