### This is a React frontend for callmemaybe - The chat app

In order to run You have to:
* Have Auth0 client app and api:
  Instructions for creating Auth0 app and api:
    - https://auth0.com/docs/get-started/auth0-overview/create-applications
    - https://auth0.com/docs/get-started/auth0-overview/set-up-apis
* Provide the `.env` file
```
REACT_APP_AUTH0_DOMAIN_URL={your-auth0-domain}.us.auth0.com
REACT_APP_AUTH0_CLIENT_ID={your-auth0-clientid}
REACT_APP_AUTH0_API_AUDIENCE={your-auth0-api-audience}
REACT_APP_API_URL={callmemaybe-api-url}
REACT_APP_API_HOST=wss://{callmemaybe-api-url}/api/v1/users/messages/ws
```
* Run with npm:
```
$ npm install
$ npm start
```